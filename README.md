### **Nama  : Rifky Zaini faroj**
### **NIM   : 1217050122**
### **Kelas   : Praktikum OOP - B**
#
# UTS

## **No.1**
https://gitlab.com/rifkyzainix/brimobile/-/blob/main/routes/web.php

## **No.2**
https://gitlab.com/rifkyzainix/brimobile/-/blob/main/routes/web.php

## **No.3**
Konsep dasar OOP (Object-Oriented Programming) adalah paradigma pemrograman yang berfokus pada pemodelan dunia nyata menggunakan objek. Berikut adalah penjelasan tentang konsep dasar OOP:

1. Objek: Objek adalah instansi dari suatu kelas. Objek memiliki atribut (properti) yang mewakili keadaan atau data yang dimiliki oleh objek tersebut, serta metode (fungsi) yang mewakili perilaku atau tindakan yang dapat dilakukan oleh objek tersebut. Misalnya, dalam konteks aplikasi BriMo, objek "Transfer" dapat memiliki properti seperti rekening asal, no tujuan, dan jumlah.

2. Kelas: Kelas adalah cetak biru atau blueprint yang mendefinisikan struktur, properti, dan metode yang akan dimiliki oleh objek-objek yang dibuat berdasarkan kelas tersebut. Kelas berfungsi sebagai panduan untuk menciptakan objek-objek dengan karakteristik yang serupa. Misalnya, kelas "Mobil" dapat memiliki properti seperti merek, model, dan tahun produksi, serta metode seperti menghidupkan mesin atau mengemudi.

3. Enkapsulasi: Enkapsulasi adalah konsep yang menggabungkan properti dan metode terkait ke dalam satu entitas tunggal yang disebut kelas. Enkapsulasi memungkinkan pengelompokan yang terorganisir dari properti dan metode yang berhubungan dengan objek tertentu, dan menyediakan kontrol akses untuk melindungi data dari akses langsung. Dengan menggunakan enkapsulasi, saya dapat menerapkan konsep seperti pengaturan (setter) dan pengambilan (getter) untuk mengatur dan mengakses nilai properti dengan cara yang terkontrol.

4. Pewarisan: Pewarisan adalah konsep yang memungkinkan kelas baru (kelas turunan) mewarisi properti dan metode dari kelas yang ada (kelas induk atau superclass). Dengan pewarisan, kelas turunan dapat memiliki properti dan metode yang sudah ada dari kelas induk, serta menambahkan properti dan metode yang unik untuk dirinya sendiri. Pewarisan memungkinkan penggunaan kembali kode, mengurangi duplikasi, dan memungkinkan pembuatan hierarki kelas yang lebih kompleks.

5. Polimorfisme: Polimorfisme adalah konsep di mana objek dari kelas yang berbeda dapat merespons metode yang sama dengan cara yang berbeda. Polimorfisme memungkinkan kelas-kelas turunan untuk mengimplementasikan metode dengan perilaku yang berbeda sesuai dengan kebutuhan mereka. Misalnya, objek "Kucing" dan objek "Anjing" dapat memiliki metode "bersuara()", tetapi masing-masing objek akan mengeluarkan suara yang berbeda.

6. Abstraksi: Abstraksi adalah konsep yang memungkinkan saya untuk memodelkan objek dan konsep kompleks ke dalam kelas-kelas yang lebih sederhana dan lebih umum. Kelas abstrak dapat menyediakan definisi umum dan metode abstrak yang harus diimplementasikan oleh kelas-kelas turunannya. Abstraksi memungkinkan saya untuk fokus pada fitur penting dan mengabaikan detail yang tidak relevan.

Dengan menggunakan konsep dasar OOP, saya dapat memecah masalah menjadi entitas-entitas yang lebih kecil dan lebih terorganisir, memungkinkan saya untuk merancang dan mengembangkan aplikasi yang lebih modular, fleksibel, dan mudah dipelihara. OOP mempromosikan pemikiran yang berorientasi pada objek, yang sejalan dengan cara saya berpikir tentang dunia nyata dan memungkinkan saya untuk memodelkannya secara lebih efektif dalam kode program.

## **No.4**
**Encapsulation**
```php
<?php
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\MutationController;
use App\Http\Controllers\PulsaController;
use App\Http\Controllers\TransferController;
use App\Http\Controllers\WithdrawController;
use Illuminate\Support\Facades\Route;

// Web Routes

Route::group(['prefix' => 'login'], function () {
   Route::get('/', [LoginController::class, 'index'])->name('login.index');
   Route::post('/', [LoginController::class, 'attempt'])->name('login.attempt');
})->middleware('guest');

Route::group(['prefix' => 'register'], function () {
   Route::get('/', [RegisterController::class, 'index'])->name('register.index');
   Route::post('/', [RegisterController::class, 'store'])->name('register.store');
})->middleware('guest');

Route::group(['middleware' => 'auth'], function () {

   Route::get('/', [HomeController::class, 'index'])->name('home');

 
   Route::group(['prefix' => 'transfer'], function () {
      Route::get('/', [TransferController::class, 'index'])->name('transfer.index');
      Route::get('/proses', [TransferController::class, 'create'])->name('transfer.create');
      Route::post('/', [TransferController::class, 'store'])->name('transfer.store');
   });

   Route::group(['prefix' => 'tarik-tunai'], function () {
      Route::get('/', [WithdrawController::class, 'create'])->name('withdraw.create');
      Route::post('/', [WithdrawController::class, 'store'])->name('withdraw.store');
      Route::get('/proses', [WithdrawController::class, 'process'])->name('withdraw.process');
      Route::post('/proses', [WithdrawController::class, 'done'])->name('withdraw.done');
   });

   Route::group(['prefix' => 'mutasi'], function () {
      Route::get('/', [MutationController::class, 'index'])->name('mutation.index');
   });

   Route::group(['prefix' => 'pulsa'], function () {
      Route::get('/', [PulsaController::class, 'create'])->name('pulsa.create');
      Route::post('/', [PulsaController::class, 'store'])->name('pulsa.store');
   });

   Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
});
```
1. Properti dan metode pada kelas-kelas seperti `LoginController`, `RegisterController`, `HomeController`, `TransferController`, `WithdrawController`, `MutationController`, dan `PulsaController` tidak diakses secara langsung dari luar kelas tersebut. Sebaliknya, mereka diakses melalui mekanisme enkapsulasi menggunakan objek dari kelas tersebut.

2. Pada setiap grup route, terdapat definisi rute yang menghubungkan URL dengan metode dalam controller. Penggunaan Route dalam framework Laravel membantu menerapkan konsep enkapsulasi dengan menyediakan cara untuk membatasi akses langsung ke metode-metode tersebut dari luar.

3. Middleware 'guest' dan 'auth' yang digunakan dalam definisi rute juga membantu melindungi akses terhadap rute tertentu berdasarkan kondisi tertentu (misalnya, hanya pengguna yang belum login yang dapat mengakses halaman login).

Dengan menggunakan enkapsulasi, kode tersebut mengatur properti dan metode terkait dalam kelas-kelas yang relevan dan membatasi akses langsung ke mereka. Hal ini memungkinkan untuk menjaga integritas data, meningkatkan keamanan, dan mengatur interaksi antara komponen-komponen dalam aplikasi secara lebih terstruktur.

## **No.5**
**Abstraction**
```php
<?php
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\MutationController;
use App\Http\Controllers\PulsaController;
use App\Http\Controllers\TransferController;
use App\Http\Controllers\WithdrawController;
use Illuminate\Support\Facades\Route;

// Web Routes

Route::group(['prefix' => 'login'], function () {
   Route::get('/', [LoginController::class, 'index'])->name('login.index');
   Route::post('/', [LoginController::class, 'attempt'])->name('login.attempt');
})->middleware('guest');

Route::group(['prefix' => 'register'], function () {
   Route::get('/', [RegisterController::class, 'index'])->name('register.index');
   Route::post('/', [RegisterController::class, 'store'])->name('register.store');
})->middleware('guest');

Route::group(['middleware' => 'auth'], function () {

   Route::get('/', [HomeController::class, 'index'])->name('home');

 
   Route::group(['prefix' => 'transfer'], function () {
      Route::get('/', [TransferController::class, 'index'])->name('transfer.index');
      Route::get('/proses', [TransferController::class, 'create'])->name('transfer.create');
      Route::post('/', [TransferController::class, 'store'])->name('transfer.store');
   });

   Route::group(['prefix' => 'tarik-tunai'], function () {
      Route::get('/', [WithdrawController::class, 'create'])->name('withdraw.create');
      Route::post('/', [WithdrawController::class, 'store'])->name('withdraw.store');
      Route::get('/proses', [WithdrawController::class, 'process'])->name('withdraw.process');
      Route::post('/proses', [WithdrawController::class, 'done'])->name('withdraw.done');
   });

   Route::group(['prefix' => 'mutasi'], function () {
      Route::get('/', [MutationController::class, 'index'])->name('mutation.index');
   });

   Route::group(['prefix' => 'pulsa'], function () {
      Route::get('/', [PulsaController::class, 'create'])->name('pulsa.create');
      Route::post('/', [PulsaController::class, 'store'])->name('pulsa.store');
   });

   Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
});
```
terdapat beberapa abstraksi yang digunakan dalam kode tersebut. Berikut adalah beberapa contoh abstraksi yang terdapat dalam kode tersebut:

1. Penggunaan Namespace:
   - Pada bagian `use`, terdapat penggunaan `use` statement untuk mengimpor namespace dari beberapa kelas yang digunakan dalam kode tersebut. Ini membantu menghindari konflik nama kelas dan mempermudah penggunaan kelas-kelas tersebut dalam kode.

2. Penggunaan Controller:
   - Kode menggunakan beberapa controller seperti `HomeController`, `LoginController`, `RegisterController`, `MutationController`, `PulsaController`, `TransferController`, dan `WithdrawController`. Controller-controller ini mewakili logika bisnis terkait dengan tindakan-tindakan yang terkait dengan rute-rute tersebut. Dengan menggunakan controller, logika terkait dapat diabstraksikan dan dikelompokkan dengan baik.

3. Penggunaan Middleware:
   - Pada beberapa grup rute, terdapat middleware yang diterapkan, seperti `'guest'` dan `'auth'`. Middleware digunakan untuk memproses permintaan sebelum mencapai tindakan yang terkait. Dalam hal ini, middleware 'guest' memastikan bahwa hanya pengguna yang belum login yang dapat mengakses halaman login dan registrasi, sementara middleware 'auth' memastikan hanya pengguna yang sudah login yang dapat mengakses rute-rute lainnya.

4. Penggunaan Route Grouping:
   - Kode menggunakan `Route::group` untuk mengelompokkan rute-rute yang memiliki pola URL atau logika terkait yang serupa. Ini membantu dalam memisahkan dan mengorganisir rute-rute yang terkait dan mempermudah pengelolaan kode.

5. Penggunaan Nama Alias Rute:
   - Setiap rute diberi nama alias menggunakan `name` untuk memudahkan referensi dan penggunaan rute tersebut dalam kode lainnya. Nama alias ini memungkinkan penggunaan `route('alias')` untuk menghasilkan URL yang sesuai dengan rute yang diberi nama.

Semua abstraksi di atas membantu memisahkan dan mengatur kode menjadi komponen-komponen yang lebih terdefinisi dan memudahkan pemeliharaan dan pengembangan kode.

## **No.6**
**Inheritance**
```php
class AccountController extends Controller
```
```php
class BalanceController extends Controller
```
Begitupun kelas pada model akan mengextends kelas model.
```php
class Account extends Model
```
Inheritance dalam pemrograman adalah sebuah konsep di mana suatu kelas dapat mewarisi properti (variabel) dan metode (fungsi) dari kelas lain. Dalam konteks yang diberikan, kelas `AccountController` dan `BalanceController` meng-extends (mewarisi) kelas `Controller`, sedangkan kelas `Account` meng-extends kelas `Model`.

Kelas `AccountController` dan `BalanceController` meng-extends kelas `Controller`, yang berarti keduanya akan mewarisi semua properti dan metode yang ada di kelas `Controller`. Ini memungkinkan `AccountController` dan `BalanceController` untuk memiliki akses dan menggunakan fungsi-fungsi yang telah didefinisikan di kelas `Controller`. Dengan menerapkan inheritance, kita dapat menghindari pengulangan kode yang tidak perlu dan memperoleh keuntungan dari reusabilitas kode.

Sementara itu, kelas `Account` meng-extends kelas `Model`. Dalam konteks _framework_ Laravel, kelas `Model` merupakan bagian dari _ORM (Object-Relational Mapping)_ yang digunakan untuk berinteraksi dengan basis data. Dengan meng-extends kelas `Model`, kelas `Account` akan mewarisi semua fitur dan fungsionalitas yang disediakan oleh kelas `Model`. Ini termasuk metode-metode untuk melakukan operasi _query_ pada tabel yang terkait dengan model `Account`, seperti pencarian, penambahan, penghapusan, dan lain-lain. Dengan menggunakan inheritance, kita dapat memanfaatkan fungsionalitas dasar yang telah disediakan oleh kelas `Model` dan menyesuaikannya sesuai kebutuhan pada kelas `Account`.

Secara keseluruhan, inheritance memungkinkan kita untuk membangun hierarki kelas yang memanfaatkan dan memperluas kode yang telah ada, sehingga mempermudah pengelolaan dan pengembangan aplikasi.

**Polymorphysm**

Pada class model yang diextends oleh model lainnya yang dibuat itu menerapkan sebuah polymorphism.

```php
abstract class Model implements Arrayable, ArrayAcces, CanBeEscapedWhenCastToString, Has BroadcastChannel, Jsonable, JsonSerializable, QueueableEntity, UrlRoutable
{
use  Concerns\HasAttributes,
		Concerns\HasEvents,
		Concerns\HasGlobalScopes,
		Concerns\HasRelationships,
		Concerns\HasTimestamps,
		Concerns\HasUniqueIds,
		Concerns\HidesAttributes,
		Concerns\GuardsAttributes,
		ForwardsCall;
		
	/**
	* The connection name for the model. 
	*
	*@var string/null
	*/
	protected $connection;
	
	/**
	* The table associated with the model.
	*
	*@var string
	*/
	protected $table;
```
Di class abstractnya terdapat properti $table dengan access modifier proctected yang mana tiap model memiliki value yang berbeda-beada untuk properti $table tersebut. Seperti contoh pada dua model berikut:
```php
class Account extends Model
{
    use HasFactory;

    protected $table = 'accounts';
    
    protected $fillable = [
        'users_id',
        'number',
        'name',
    ];
}
```
```php
class Balance extends Model
{
    use HasFactory;

    protected $table = 'balances';

    protected $fillable = [
        'accounts_number',
        'amount',
        'timestamp',
    ];
}
```
Pada kelas Account, properti $table bernilai “accounts” sementara pada Balance, properti $table bernilai “balances”.
## **No.7**
**Use Case Pengguna**
 **No** | **Use Case**                                                 | **id** | **Level** 
--------|--------------------------------------------------------------|--------|-----------
 **1**  | User dapat melakukan register/daftar akun BriMobile          | 464601 | High      
 **2**  | User dapat melakukan login setelah daftar akun BriMobile     | 464602 | High      
 **3**  | User dapat mengetahui nomor rekening di halaman dasboard     | 464603 | Medium    
 **4**  | User dapat mengetahui saldo saat ini di halaman dasboard     | 464604 | High      
 **5**  | User dapat melakukan transfer uang sejumlah yang di inputkan | 464605 | High      
 **6**  | User dapat melakukan tarik tunai sesuai nominal yang dipilih | 464606 | Medium    
 **7**  | User dapat melihat mutasi atau riwayat transaksi             | 464607 | Medium    
 **8**  | User dapat melakukan pembelian pulsa                         | 464608 | Low       
 **9**  | User dapat melihat pemasukan dan pengeluaran saldo           | 464609 | Medium    
 **10** | User dapat logout dari aplikasi BriMobile                    | 464610 | Medium    

## **No.8**
https://gitlab.com/rifkyzainix/brimobile/-/blob/main/ClassDiagram.png

Diagram class tersebut menggambarkan hubungan antara beberapa kelas yang terlibat dalam suatu sistem atau aplikasi. Berikut adalah penjelasan untuk setiap kelas:

1. **Account**:
   - Memiliki atribut `users_id`, `number`, dan `name` yang memiliki modifier private (`-`).
   - Memiliki method `get_users_id()`, `set_users_id(users_id)`, `get_number()`, `set_number(number)`, `get_name()`, dan `set_name(name)` untuk mengakses dan mengatur nilai atribut.

2. **User**:
   - Memiliki atribut `id_number`, `name`, `email`, dan `password` yang memiliki modifier private (`-`).
   - Memiliki method `get_id_number()`, `set_id_number(id_number)`, `get_name()`, `set_name(name)`, `get_email()`, `set_email(email)`, `get_password()`, dan `set_password(password)` untuk mengakses dan mengatur nilai atribut.

3. **Balance**:
   - Memiliki atribut `accounts_number`, `amount`, dan `timestamp` yang memiliki modifier private (`-`).
   - Memiliki method `get_accounts_number()`, `set_accounts_number(accounts_number)`, `get_amount()`, `set_amount(amount)`, `get_timestamp()`, dan `set_timestamp(timestamp)` untuk mengakses dan mengatur nilai atribut.

4. **Transaction**:
   - Memiliki atribut `accounts_number`, `type`, `amount`, `status`, `recipient`, dan `timestamp` yang memiliki modifier private (`-`).
   - Memiliki method `get_accounts_number()`, `set_accounts_number(accounts_number)`, `get_type()`, `set_type(type)`, `get_amount()`, `set_amount(amount)`, `get_status()`, `set_status(status)`, `get_recipient()`, `set_recipient(recipient)`, `get_timestamp()`, dan `set_timestamp(timestamp)` untuk mengakses dan mengatur nilai atribut.

5. **Pulsa**:
   - Memiliki atribut `transactions_id`, `phone_number`, dan `timestamp` yang memiliki modifier private (`-`).
   - Memiliki method `get_transactions_id()`, `set_transactions_id(transactions_id)`, `get_phone_number()`, `set_phone_number(phone_number)`, `get_timestamp()`, dan `set_timestamp(timestamp)` untuk mengakses dan mengatur nilai atribut.

6. **Withdraw**:
   - Memiliki atribut `transactions_id`, `code`, `expired`, dan `timestamp` yang memiliki modifier private (`-`).
   - Memiliki method `get_transactions_id()`, `set_transactions_id(transactions_id)`, `get_code()`, `set_code(code)`, `get_expired()`, `set_expired(expired)`, `get_timestamp()`, dan `set_timestamp(timestamp)` untuk mengakses dan mengatur nilai atribut.

Hubungan antara kelas-kelas tersebut ditunjukkan oleh panah (relationship arrow) pada diagram. Beberapa hubungan yang tergambar adalah:

- Kelas **Account** memiliki hubungan one-to-many (1 - *) dengan kelas **User**. Ini berarti satu pengguna dapat memiliki banyak akun.
- Kelas **Balance** memiliki hubungan one-to-many (1 - *) dengan kelas **Transaction**. Ini berarti satu saldo dapat terhubung dengan banyak transaksi.
- Kelas **Pulsa** memiliki hubungan one-to-many (1 - *) dengan kelas **Withdraw**. Ini berarti satu transaksi pulsa dapat memiliki banyak penarikan.

Harap dicatat bahwa hubungan tersebut didasarkan pada asumsi bahwa atribut-atribut yang sesuai digunakan sebagai kunci hubungan, seperti `users_id`, `accounts_number`, dan `transactions_id`. Jika ada atribut lain yang digunakan sebagai kunci hubungan, maka hubungan dapat berubah sesuai dengan penggunaan atribut tersebut.
## **No.9**
## **No.10**
https://gitlab.com/rifkyzainix/brimobile/-/blob/main/ux1.png

#
#
#
# **UAS**

## **No.1**
**Use Case Pengguna**
 **No** | **Use Case**                                                 | **id** | **Level** 
--------|--------------------------------------------------------------|--------|-----------
 **1**  | User dapat melakukan register/daftar akun BriMobile          | 464601 | High      
 **2**  | User dapat melakukan login setelah daftar akun BriMobile     | 464602 | High      
 **3**  | User dapat mengetahui nomor rekening di halaman dasboard     | 464603 | Medium    
 **4**  | User dapat mengetahui saldo saat ini di halaman dasboard     | 464604 | High      
 **5**  | User dapat melakukan transfer uang sejumlah yang di inputkan | 464605 | High      
 **6**  | User dapat melakukan tarik tunai sesuai nominal yang dipilih | 464606 | Medium    
 **7**  | User dapat melihat mutasi atau riwayat transaksi             | 464607 | Medium    
 **8**  | User dapat melakukan pembelian pulsa                         | 464608 | Low       
 **9**  | User dapat melihat pemasukan dan pengeluaran saldo           | 464609 | Medium    
 **10** | User dapat logout dari aplikasi BriMobile                    | 464610 | Medium    

**Use Case Manajemen Perusahaan**
 **No** | **Use Case**                    | **Deskripsi**                                                                                                                                                                                                                                    
--------|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 **1**  | Pengelolaan Keuangan Perusahaan | BRI Mobile memungkinkan pengguna untuk mengakses dan mengelola rekening bisnis mereka, termasuk memeriksa saldo, melihat riwayat transaksi, dan mentransfer dana antar rekening                                                                  
 **2**  | Pembayaran Gaji Karyawan        | Perusahaan dapat menggunakan BRI Mobile untuk melakukan pembayaran gaji kepada karyawan dengan cepat dan aman melalui fitur transfer antarbank                                                                                                   
 **3**  | Pembayaran Tagihan              | Manajemen perusahaan dapat menggunakan BRI Mobile untuk membayar berbagai tagihan perusahaan seperti tagihan listrik, telepon, air, atau tagihan lainnya melalui fitur pembayaran tagihan                                                        
 **4**  | Manajemen Rekening              | Dengan BRI Mobile, perusahaan dapat melacak dan mengelola rekening bank mereka dengan mudah, termasuk menambahkan rekening baru, mengganti status rekening, atau mengatur batasan transaksi                                                      
 **5**  | Penjadwalan Pembayaran          | Perusahaan dapat menggunakan fitur penjadwalan pembayaran dalam BRI Mobile untuk mengatur pembayaran rutin seperti sewa, cicilan, atau pembayaran lainnya agar otomatis dilakukan pada tanggal yang ditentukan                                   
 **6**  | Verifikasi Transaksi            | Dalam manajemen keuangan perusahaan, BRI Mobile dapat digunakan untuk melakukan verifikasi transaksi melalui kode otentikasi yang dikirim melalui SMS atau notifikasi push                                                                       
 **7**  | Manajemen Kredit                | Jika perusahaan memiliki fasilitas kredit dengan Bank BRI, BRI Mobile memungkinkan manajemen perusahaan untuk memantau dan mengelola kredit bisnis mereka, termasuk melihat saldo kredit, jadwal pembayaran, atau permintaan perpanjangan kredit 
 **8**  | Pemesanan dan Pembayaran Produk | Jika perusahaan beroperasi dalam bidang e\-commerce atau memiliki katalog produk, BRI Mobile dapat digunakan oleh pelanggan untuk melakukan pemesanan dan pembayaran produk dengan cepat dan mudah                                               
 **9**  | Pelaporan Keuangan              | BRI Mobile menyediakan akses cepat ke informasi keuangan perusahaan, termasuk laporan mutasi rekening, rekening koran, dan ringkasan aktivitas keuangan lainnya yang berguna untuk manajemen perusahaan                                          
 **10** | Notifikasi dan Pemberitahuan    | BRI Mobile dapat mengirimkan notifikasi dan pemberitahuan penting kepada manajemen perusahaan, seperti pemberitahuan transaksi yang mencurigakan, peningkatan saldo rekening, atau informasi terkait produk dan layanan perbankan                

**Use case direksi perusahaan (dashboard, monitoring, analisis)**
 **No** | **Use Case**                  | **Deskripsi**                                                                                                                                                                                                                                                                                                                       
--------|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 **1**  | Dashboard Keuangan            | BRI Mobile dapat menyediakan dashboard yang memungkinkan saya melihat gambaran keseluruhan keuangan perusahaan secara langsung\. saya dapat melihat saldo rekening, riwayat transaksi, perkiraan pendapatan, dan pengeluaran perusahaan dalam satu tampilan yang mudah dipahami                                                     
 **2**  | Monitoring Transaksi          | saya dapat menggunakan BRI Mobile untuk memonitor transaksi perusahaan secara real\-time\. Misalnya, saya dapat melihat transaksi yang sedang berlangsung, melacak pembayaran yang belum diselesaikan, atau memeriksa transaksi yang memerlukan persetujuan dari pihak direktur                                                     
 **3**  | Analisis Keuangan             | BRI Mobile dapat membantu saya melakukan analisis keuangan yang mendalam\. saya dapat mengakses laporan keuangan perusahaan, melihat tren pendapatan dan pengeluaran, membandingkan kinerja keuangan dari periode ke periode, atau menganalisis rasio keuangan untuk mendapatkan wawasan yang lebih baik tentang kondisi perusahaan 
 **4**  | Pemantauan Kredit             | Jika perusahaan saya memiliki fasilitas kredit dengan Bank BRI, saya dapat menggunakan BRI Mobile untuk memantau status kredit, melihat jadwal pembayaran, atau memeriksa ketersediaan limit kredit yang tersedia                                                                                                                   
 **5**  | Notifikasi Penting            | BRI Mobile dapat memberikan notifikasi langsung kepada saya sebagai direktur perusahaan\. Misalnya, saya dapat menerima pemberitahuan segera ketika transaksi besar dilakukan, ada perubahan signifikan dalam saldo rekening, atau ketika ada kegiatan keuangan yang memerlukan tindakan atau persetujuan dari saya                 
 **6**  | Pengaturan dan Pembaruan Akun | BRI Mobile memungkinkan saya untuk mengelola dan memperbarui informasi akun perusahaan saya, seperti mengubah kata sandi atau melakukan perubahan pada profil pengguna                                                                                                                                                              
 **7**  | Akses Informasi Pasar         | Jika saya tertarik dengan informasi pasar terkait bisnis saya, BRI Mobile dapat menyediakan akses ke berita dan informasi keuangan terkini\. saya dapat mengikuti perkembangan ekonomi, melacak indeks saham, atau mengakses analisis pasar yang relevan                                                                            

## **No.2**
https://gitlab.com/rifkyzainix/brimobile/-/blob/main/ClassDiagram.png
#
## **No.3**
```php
<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\MutationController;
use App\Http\Controllers\PulsaController;
use App\Http\Controllers\TransferController;
use App\Http\Controllers\WithdrawController;
use Illuminate\Support\Facades\Route;

// Web Routes

Route::group(['prefix' => 'login'], function () {
    Route::get('/', [LoginController::class, 'index'])->name('login.index');
    Route::post('/', [LoginController::class, 'attempt'])->name('login.attempt');
})->middleware('guest');

Route::group(['prefix' => 'register'], function () {
    Route::get('/', [RegisterController::class, 'index'])->name('register.index');
    Route::post('/', [RegisterController::class, 'store'])->name('register.store');
})->middleware('guest');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [HomeController::class, 'index'])->name('home');

    Route::group(['prefix' => 'transfer'], function () {
        Route::get('/', [TransferController::class, 'index'])->name('transfer.index');
        Route::get('/proses', [TransferController::class, 'create'])->name('transfer.create');
        Route::post('/', [TransferController::class, 'store'])->name('transfer.store');
    });

    Route::group(['prefix' => 'tarik-tunai'], function () {
        Route::get('/', [WithdrawController::class, 'create'])->name('withdraw.create');
        Route::post('/', [WithdrawController::class, 'store'])->name('withdraw.store');
        Route::get('/proses', [WithdrawController::class, 'process'])->name('withdraw.process');
        Route::post('/proses', [WithdrawController::class, 'done'])->name('withdraw.done');
    });

    Route::group(['prefix' => 'mutasi'], function () {
        Route::get('/', [MutationController::class, 'index'])->name('mutation.index');
    });

    Route::group(['prefix' => 'pulsa'], function () {
        Route::get('/', [PulsaController::class, 'create'])->name('pulsa.create');
        Route::post('/', [PulsaController::class, 'store'])->name('pulsa.store');
    });

    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
});
```
1. Single Responsibility Principle (SRP):
Setiap controller (misalnya HomeController, LoginController, RegisterController, dll.) bertanggung jawab untuk menangani permintaan terkait dengan fungsi khusus yang diwakilinya. Misalnya, LoginController bertanggung jawab untuk mengelola proses login, RegisterController bertanggung jawab untuk mengelola proses registrasi, dan seterusnya. Setiap controller memiliki tanggung jawab tunggal yang terpisah dari yang lain.

2. Open/Closed Principle (OCP):
Dalam kode tersebut, tidak ada perubahan langsung pada rute yang ada saat menambahkan atau memperluas fitur baru. Sebagai gantinya, rute baru ditambahkan dalam grup yang sesuai. Misalnya, ketika ingin menambahkan fitur "mutasi" atau "pulsa", saya menambahkan grup rute baru dan menghubungkannya dengan controller yang relevan. Ini mengikuti prinsip OCP, di mana kode dapat diperluas dengan menambahkan fungsionalitas baru tanpa memodifikasi kode yang sudah ada.

3. Liskov Substitution Principle (LSP):
Dalam kode tersebut, saya menggunakan kelas-kelas controller yang diturunkan dari Controller di dalam Laravel. Kode ini mematuhi LSP, yang berarti objek dari kelas turunan dapat digunakan sebagai pengganti objek dari kelas induk tanpa mempengaruhi kebenaran atau konsistensi sistem.

4. Interface Segregation Principle (ISP):
Dalam kode tersebut, tidak ada penggunaan langsung terhadap prinsip ISP, karena tidak ada definisi langsung tentang antarmuka. Namun, dalam konteks framework Laravel, konsep ISP diterapkan secara internal dengan memanfaatkan antarmuka dan kontrak untuk komunikasi antara komponen-komponen sistem. Hal ini memungkinkan adanya pemisahan antarmuka yang spesifik dan terpisah untuk setiap komponen.

5. Dependency Inversion Principle (DIP):
Dalam kode tersebut, dependensi controller terbalik melalui penggunaan dependency injection melalui konstruktor controller. Misalnya, dalam route '/transfer', controller TransferController dihubungkan menggunakan sintaks [TransferController::class, 'index']. Dengan menggunakan dependency injection, dependensi controller disediakan dari luar (dalam hal ini, oleh framework Laravel) melalui konstruktor controller, mengikuti prinsip DIP.

## **No.4**
Design Pattern yang digunakan adalah MVC (model, view, controller).

![Design Pattern](https://drive.google.com/file/d/1S-KOhA2CrN2_8mwK3uyEAXVrR8aloNA6/view?usp=sharing)

Model digunakan untuk representasi data dan juga untuk interaksi dengan database. Bisa dibilang ini adalah bentuk tranformasi dari database menjadi sebuah object.

![models](https://drive.google.com/file/d/1heqUM046rgo4plk158PKFsTU0L_yAtE3/view?usp=sharing)

View digunakan untuk menyajikan data atau menampilkan sebuah tampilan dari web yang dibuat, bisa juga dibilang kalau ini adalah bagian frontend dari sebuah web.

![views](https://drive.google.com/file/d/1ye3DWUmgffTJpVvYcC0DxRxWRxu4CaQd/view?usp=sharing)

Dalam pembuatan aplikasi menggunakan laravel, controller digunakan untuk business logic, berinteraksi dengan model untuk mengolah data yang didapat dari database, juga untuk berinteraksi dengan views guna menyajikan tampilan. Bisa dibilang seperti otaknya controller ini yang mengatur proses yang terjadi pada website.

Tiap function di controller bertugas untuk suatu route tertentu, dan juga controller dapat mengarahkan route tujuan mau kemana.


## **No.5**
Database yang digunakan pada aplikasi ini adalah berbasis SQL, tepatnya mysql, karena struktur data dari aplikasi bri mobile ini selalu tetap dan tidak sampai membutuhkan nosql. 
Konektivitas ke database di laravel sudah diatur oleh frameworknya sendiri, jadi hanya perlu mengisi data pada file .env.
```sql
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=brimo
DB_USERNAME=root
DB_PASSWORD=
```
Data yang perlu diisi hanyalah seperti diatas, jenis database nya apa, lalu host dan port nya, nama database serta username dan password dari databasenya.
Karena untuk mysql clinet nya menggunakan xampp, sehingga perlu menyalakan MySQL di xampp nya terlebih dahulu.

Untuk mengecek apakah sudah terkoneksi atau belum sebenanrya hanya perlu menjalankan aplikasi laravelnya menggukana perintah : php artisan serve. Jika sudah terhubung maka tidak akan ada pesan error yang berkaitan dengan databasenya.

## **No.6**
Pembuatan aplikasi laravel ini untuk membuat project baru memerlukan software composer sehingga harus menginstallnya terlebih dahulu.
Jika sudah, maka buat sebuah folder dengan nama brimo. Kemudian buka terminal dan arahkan ke direktori tersebut.
Jalankan perintah berikut untuk membuat project baru:
```php
composer create-project laravel/laravel .
```
Yang harus dibuat selanjutnya adalah Migrasi, Model dan Controller, dalam laravel ada perintah yang singkat untuk membuatnya sekaligus:
```php
php artisan make:model Account -mcr
php artisan make:model Balance -m
php artisan make:model Pulsa -mc
php artisan make:model Transaction -m
php artisan make:model User -m
php artisan make:model Withdraw -mc
```
Lalu membuat beberapa controller :
```php
php artisan make:controller HomeController
php artisan make:controller LoginController
php artisan make:controller MutationController
php artisan make:controller RegisterController
php artisan make:controller TransferController
```
Untuk menjalan awal :
```php
php artisan migrate:fresh –seed
```
Untuk Isi dari masing-masing migrasi, model dan juga controller dapat dilihat pada source code.
Proses CRUD terdapat pada controller dan menggunakan Eloquent ORM atau ORM yang disediakan oleh framework laravel. ORM atau Object Relational Mapping itu sederhananya adalah sebuah teknik yang menghubungkan antara basis data relasional dengan objek dalam pemrograman berorientasi objek.
#
**create**
```php
public function store(Request $request)
    {
        $validatedData = $request->validate([
            'users_id' => 'required',
            'number' => 'required|unique:accounts',
            'name' => 'required',
        ]);

        Account::create($validatedData);

        return redirect()->route('accounts.index')->with('success', 'Account created successfully.');
    }
```
Pada controller yang sama tetapi fungsi store itu terdapat proses Create yang menggunakan model juga, jadi hanya perlu menggunakan fungsi create.
Jika diubah menjadi syntax SQL nya akan seperti berikut:
INSERT INTO accounts (users_id, number, name) VALUES (‘’,’’,’’);
#
**read**
```php
class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $accounts = Account::all();

        return view('accounts.index', compact('accounts'));
    }
```
Proses Read dapat dilihat pada salah satu function di AccountController yaitu mengambil semua data dari tabel accounts melalui Model Account. Semua proses crud pada aplikasi ini menggunakan model sebagai perantara nya, jadi tidak langsung menuliskan query nya.
Jika diubah menjadi syntax sql nya, menjadi:

```sql
SELECT * FROM accounts;
```
#
**update**
```php
public function done(Request $request): RedirectResponse
    {
        $transaction = Transaction::find($request->id);
        $transaction->status = 'completed';
        $transaction->save();

        return redirect()->route('home');
    }
```
Pada WithdrawController terdapat fungsi done yang melakukan update status pada table transactions melalui model. Jika diubah menjadi SQL syntax akan seperti berikut:

```sql
UPDATE transactions SET status=’completed’ WHERE id=’’;
```
#
**delete**
```php
 public function destroy(Account $account)
    {
        $account->delete();

        return redirect()->route('accounts.index')->with('success', 'Account deleted successfully.');
    }
}
```
Pada AccountController terdapat fungsi destroy yang didalam nya bertugas untuk melakukan delete pada sebuah data di table accounts melalui model. Syntax sql akan seperti berikut:
```sql
DELETE FROM accounts WHERE id=’’;
```
#
## **No.7**
https://gitlab.com/rifkyzainix/brimobile/-/blob/main/ux1.png

Itu adalah contoh tampilan halaman login yang berupa GUI atau Graphical User Interface yang dibuat pada aplikasi ini berbasis web menggunakan html, css dan javascript, tepatnya framework bootstrap dan juga laravel blade template.
GUI singkatnya adalah tampilan antarmuka yang menggunakan elemen visual sebagai sarana user berinteraksi dengan software. GUI pada aplikasi berbasis web umumnya akan selalu menggunakan HTML dan CSS, bisa juga menambahkan Javascript.
Pada laravel terdapat blade template yang disediakan oleh framework tersebut untuk memudahkan pembuatan GUI pada aplikasi sehingga tidak terlalu statis seperti html biasa. Konsep reusability juga terdapat pada blade template karena kita bisa menggunakan elemen yang sama berulang kali tanpa perlu menuliskan ulang elemen tersebut, cukup memanggilnya saja.
Berikut contoh blade template.
```html
<!DOCTYPE html>
<html>

<head>
    <title>BRI MOBILE</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>

<body>
<div class="container">
    @yield('content')
</div>
</body>

</html>
```

```html
@extends('layouts.app')

@section('content')
    <div class="row" style="background-color:#1f8fe5;">
        <div class="col-12 p-2" style="box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);">
            <h5 class="text-center text-white">BRI mo</h5>
        </div>
    </div>
    <div class="row" style="padding-bottom: 53%;background-color:#1f8fe5;border-radius: 0 0 45% 45%;">
        <div class="col-12 pt-4">
            <h6 class="text-white text-center">Selamat Datang</h6>
        </div>
    </div>
    <div class="row justify-content-center" style="background-color: #fff;">
        <div class="col-lg-6 col-md-8 col-sm-12 pt-3">
            @if(session()->has('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @elseif(session()->has('errors'))
```
Terlihat kalau disitu ada @extends untuk mengextends elemen layouts. @yields untuk mebuat sebuah template dimana yang didalam yields itu akan dapat berubah atau dinamis.
Terdapat juga beberapa kode php yang dibuat lebih mudah penulisannya di blade template ini.

Seperti if yang hanya perlu menulisan @if, lalu untuk menampilkan data hanya perlu menuliskan di dalam sebuah double kurung kurawal {{ “yang mau ditampilkan” }}.

#
## **No.8**
HTTP connection adalah sebuah koneksi http atau sering digunakan untuk mentransfer data antara client dan server. Mudahnya koneksi http ini adalah sebuah cara untuk proses pertukaran data antar client dan server.
Pada tampilan login diatas, ketika mengklik tombol login, maka client mengirimkan sebuah data melalui http ke server dan diproses oleh server.
```html
 <form method="POST" action="{{ route('login.attempt') }}">
                @csrf
                <div class="row justify-content-center" style="background-color: #fff;padding-bottom: 10%;">
                    <div  class="col-lg-6 col-md-8 col-sm-12 px-3">
                        <div class="form-group">
                            <label><b style="color: #05539b;font-weight: bold;">Login</b></label>
                            <input type="email" class="form-control mt-3" name="email" placeholder="example@mail.com" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
```
Itu adalah contoh code pada halaman client untuk mengirim sebuah data ke server menggunakan method POST. Ada juga method GET yang dapat digunakan untuk mengirim ke server, selain itu sebenarnya terdapat beberapa method lain, tapi yang umum digunakan adalah GET, POST, PUT dan DELETE.
GET biasa digunakan untuk meminta data, POST untuk mengirim data, PUT untuk mengedit data, DELETE untuk menghapus data.

Perbedaannya pada GET dan POST itu kalau get di url akan terlihat data yang dikirimkan user ke server, tetapi method POST tidak. Begitu juga method PUT dan DELETE tidak terlihat.

```php
 public function attempt(Request $request): RedirectResponse
    {
        try {
            $credentials = $request->only('email', 'password');

            if (auth()->attempt($credentials)) {
                $account_number = Account::where('users_id', auth()->id())
                    ->select('number')->first();
                session(['accounts_number' => $account_number->number]);
                return redirect()->route('home');
            }
```
Itu adalah contoh code pada server menerima data yang dikirimkan oleh client melalui http. $request itu adalah variabel yang mebawa data yang dikirim oleh client.
#
## **No.9**
#
## **No 10**
```
```


